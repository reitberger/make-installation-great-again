# Installation files for nvidia-cuda, common packages for pip and ubuntu/arch linux
### Installation
```
git clone https://gitlab.com/reitberger/make-installation-great-again.git
cd make-installation-great-again
./install_all.sh
```
## NOTES:
### Only use install_all.sh in your cloned git directory (because of pwd)
### It will overwrite ~/.zshrc for manual installation remove source files in install_all
