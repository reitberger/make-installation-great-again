#!/bin/sh
debian_install(){
    sudo apt-get -y install linux-tools-common docker docker-compose git ssh python3-pip zsh
    systemd_config
}
arch_install(){
    sudo pacman -S base-devel docker git openssh python-pip zsh
    systemd_config
}
systemd_config(){
    sudo systemctl enable --now docker
    docker_setup
    if [ "$ENABLESSH" == "y" ]; then
        sudo systemctl enable --now ssh
    else
        sudo systemctl disable --now ssh
    fi
}
docker_setup(){
    sudo groupadd docker
    sudo usermod -aG docker ${USER}
}
case $manager in 1) arch_install;;
    2) debian_install;;
*)
esac
