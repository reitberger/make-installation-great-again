#!/bin/sh
arch_install(){
    sudo pacman -S cuda cuda-tools python-pip
}
debian_install(){
    sudo add-apt-repository ppa:graphics-drivers/ppa
    sudo apt-get -y install nvidia-cuda-toolkit python3-pip
}
case $manager in 1) arch_install;;
    2) debian_install;;
*)
esac
