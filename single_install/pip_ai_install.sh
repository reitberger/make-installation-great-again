#!/bin/sh
# maybe non exhaustive -needs testing
python_install(){
    pip3 install numpy pandas matplotlib wordcloud umap-learn sentence_transformers scipy sklearn \
    keybert plotly tensorflow
    pip3 install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu116
}
python_install
