#!/bin/env bash
UTILS=$PWD/utils
INSTALL=$PWD/single_install

# sets $manager variable which later scripts will use to install from correct package manager
source $UTILS/get_package_manager.sh
# ask for setup of ssh -> common_packages depends on it
source $UTILS/ask_enable_ssh.sh

# installs mainly cuda -> depends on $manager being set
source $INSTALL/cuda_install.sh
# installs and configures common packages like docker -> depends on $manager being set
source $INSTALL/common_packages.sh
# installs ai libraries
source $INSTALL/pip_ai_install.sh
# overwrite ~/.zshrc with grml's default config (also used as default in arch installation media)
read manager
wget -O ~/.zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
